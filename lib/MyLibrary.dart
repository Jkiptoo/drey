import 'package:drey/BookLibrary.dart';
import 'package:drey/Settings.dart';
import 'package:drey/SettingsPage.dart';
import 'package:drey/WelcomePage.dart';
import 'package:flutter/material.dart';
import 'package:drey/AddBookLib.dart';
import 'package:drey/AddUser.dart';



class MyLibrary extends StatelessWidget {
  final appTitle = 'Settings';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: appTitle,
      home: MyHomePage(title: appTitle),
    );
  }
}

class MyHomePage extends StatelessWidget {
  final String title;

  MyHomePage({Key key, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(title),backgroundColor: Colors.teal[800],),
      body:  SingleChildScrollView(child:
      Column(children: <Widget>[
        SizedBox(height: 20,),
        Column(children: <Widget>[
          Row(children: <Widget>[
            SizedBox(width: 25,),
            Material(child: InkWell(
              onTap: (){},
              child: Column(children: <Widget>[
                Text('300',style: TextStyle(fontSize: 25,fontWeight: FontWeight.bold),),
                Text('All Books',style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold,color: Colors.pinkAccent))
              ],),
            ),),
            SizedBox(width: 50,),
            Material(child: InkWell(
              onTap: (){},
              child: Column(children: <Widget>[
                Text('30',style: TextStyle(fontSize: 25,fontWeight: FontWeight.bold)),
                Text('Books Out',style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold))
              ],),
            ),),

            SizedBox(width: 50,),
            Material(child: InkWell(
              onTap: (){},
              child: Column(children: <Widget>[
                Text('3',style: TextStyle(fontSize: 25,fontWeight: FontWeight.bold)),
                Text('Lost Books',style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold, color: Colors.green))
              ],),
            ),),

          ],),
          Image(fit: BoxFit.fitWidth, image: AssetImage('assets/dashboard.png'))
        ],),

        Row(children: <Widget>[
          SizedBox(width: 30,),
          Text('Borrowing History',style: TextStyle(fontWeight: FontWeight.bold),)
        ],),
        Container(
          height: 105,
          child: Card(child: Row(children: <Widget>[
            SizedBox(width: 30,),
            Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
              Text('Issued to ',style: TextStyle(fontSize: 7)),
              Text('Date of Issue',style: TextStyle(fontSize: 7)),
              Text('Book ID',style: TextStyle(fontSize: 7)),
              Text('Title',style: TextStyle(fontSize: 7)),
              Text('Author',style: TextStyle(fontSize: 7)),
              Text('Notes',style: TextStyle(fontSize: 7)),
              Text('Status',style: TextStyle(fontSize: 7))
            ],),
            SizedBox(width: 20,),
            Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
              Text('Charity Wayua',style: TextStyle(fontSize: 7)),
              Text('31-11-2019',style: TextStyle(fontSize: 7)),
              Text('KJM-278',style: TextStyle(fontSize: 7)),
              Text('Chase The Lion',style: TextStyle(fontSize: 7)),
              Text('Mark Batterson',style: TextStyle(fontSize: 7)),
              Text('Book issued in good condition',style: TextStyle(fontSize: 7)),
              ButtonTheme(minWidth: 50, height: 5, child: FlatButton(
                  color: Colors.orange,
                  onPressed: (){}, child: Text('OverDue')),)
            ],),
            SizedBox(width: 70,),
            Image(image: AssetImage('assets/extra.jpg'))
          ],),),),
        Container(height: 105, child: Card(child: Row(children: <Widget>[
          SizedBox(width: 30,),
          Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
            Text('Issued ',style: TextStyle(fontSize: 7),),
            Text('Date of Issue',style: TextStyle(fontSize: 7),),
            Text('Book ID',style: TextStyle(fontSize: 7),),
            Text('Title',style: TextStyle(fontSize: 7),),
            Text('Author',style: TextStyle(fontSize: 7),),
            Text('Notes',style: TextStyle(fontSize: 7),),
            Text('Status',style: TextStyle(fontSize: 7),)
          ],),
          SizedBox(width: 20,),
          Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
            Text('Charity Wayua',style: TextStyle(fontSize: 7),),
            Text('31-11-2019',style: TextStyle(fontSize: 7),),
            Text('KJM-278',style: TextStyle(fontSize: 7),),
            Text('Chase The Lion',style: TextStyle(fontSize: 7),),
            Text('Mark Batterson',style: TextStyle(fontSize: 7),),
            Text('Book issued in good condition',style: TextStyle(fontSize: 7),),
            ButtonTheme(minWidth: 50,height: 5, child: FlatButton(
                color: Colors.lightGreen,
                onPressed: (){}, child: Text('Returned')),)
          ],),
          SizedBox(width: 70,),
          Image(image: AssetImage('assets/mee.jpg'))
        ],),)
          ,),
        Container(height: 105, child: Card(child:  Row(children: <Widget>[
          SizedBox(width: 30,),
          Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
            Text('Issued to',style: TextStyle(fontSize: 7),),
            Text('Date of Issue',style: TextStyle(fontSize: 7),),
            Text('Book ID',style: TextStyle(fontSize: 7),),
            Text('Title',style: TextStyle(fontSize: 7),),
            Text('Author',style: TextStyle(fontSize: 7),),
            Text('Notes',style: TextStyle(fontSize: 7),),
            Text('Status',style: TextStyle(fontSize: 7),)
          ],),SizedBox(width: 20,),
          Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
            Text('Charity Wayua',style: TextStyle(fontSize: 7),),
            Text('31-11-2019',style: TextStyle(fontSize: 7),),
            Text('KJM-278',style: TextStyle(fontSize: 7),),
            Text('Chase The Lion',style: TextStyle(fontSize: 7),),
            Text('Mark Batterson',style: TextStyle(fontSize: 7),),
            Text('Book issued in good condition',style: TextStyle(fontSize: 7),),
            ButtonTheme(minWidth: 50,height: 5, child: FlatButton(
                color: Colors.orange,
                onPressed: (){}, child: Text('OverDue')),)

          ],),
          SizedBox(width: 70,),
          Image(image: AssetImage('assets/extra.jpg'))
        ],),)
          ,),
        Container(height: 105, child:
        Card(child: Row(children: <Widget>[
          SizedBox(width: 30,),
          Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
            Text('Issued to',style: TextStyle(fontSize: 7),),
            Text('Date of Issue',style: TextStyle(fontSize: 7),),
            Text('Book ID',style: TextStyle(fontSize: 7),),
            Text('Title',style: TextStyle(fontSize: 7),),
            Text('Author',style: TextStyle(fontSize: 7),),
            Text('Notes',style: TextStyle(fontSize: 7),),
            Text('Status',style: TextStyle(fontSize: 7),)
          ],),SizedBox(width: 20,),
          Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[

            Text('Charity Wayua',style: TextStyle(fontSize: 7),),
            Text('31-11-2019',style: TextStyle(fontSize: 7),),
            Text('KJM-278',style: TextStyle(fontSize: 7),),
            Text('Chase The Lion',style: TextStyle(fontSize: 7),),
            Text('Mark Batterson',style: TextStyle(fontSize: 7),),
            Text('Book issued in good condition',style: TextStyle(fontSize: 7),),
            ButtonTheme(minWidth: 50, height: 2,child: FlatButton(
                color: Colors.orange,
                onPressed: (){}, child: Text('OverDue')),)

          ],),
          SizedBox(width: 70,),
          Image(image: AssetImage('assets/extra.jpg'))
        ],),),),
        SizedBox(height: 20,),
        BottomNavigationBar(items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(title: Text(''), icon: Material(
            child: InkWell(
              onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (context)=> WelcomePage()));
              },
              child: Image(height: 30, image: AssetImage('assets/home.png')),
            ),
          )
          ),
          BottomNavigationBarItem(title: Text(''), icon: Material(
            child: InkWell(
              onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (context)=> BookLibrary()));
              },
              child: Image(height: 30,image: AssetImage('assets/library.png')),
            ),
          )),
          BottomNavigationBarItem(title: Text(''), icon: Material(
            child: InkWell(
              onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (context)=> AddBookLib()));
              },
              child: Image(height: 30,image: AssetImage('assets/add.png')),
            ),
          ),

          ),

        ]),


      ],),),
      drawer: Drawer(

        child: ListView(

          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              child: Column(children: <Widget>[
                CircleAvatar(
                  radius: 55,
                  child: Image.asset('assets/choge.png'),
                ),

                Text('Charity Wangui',style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),)
              ],),
              decoration: BoxDecoration(
              ),
            ),
            ListTile(
              title: Text('Book Library'),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context)=> BookLibrary()));
              },
            ),
            ListTile(
              title: Text('Add Book'),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context)=> AddBookLib()));
              },
            ),
            ListTile(
              title: Text('Issue List'),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context)=> CustomData()));
              },
            ),
            ListTile(
              title: Text('Lost Books'),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context)=> Settings()));
              },
            ),
            ListTile(
              title: Text('Settings'),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context)=> SettingsPage()));
              },
            ),
            ListTile(
              title: Text('Help & feedback'),
              onTap: () {
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: Text('About'),
              onTap: () {
                Navigator.pop(context);
              },
            ),
          ],
        ),
      ),
    );
  }
}
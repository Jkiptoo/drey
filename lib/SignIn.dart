import 'package:flutter/material.dart';
import 'package:drey/SignInPage.dart';


class SignIn extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(

        home: Scaffold(
            body: SingleChildScrollView(child:
            Stack(
                children: <Widget>[
                  Card(elevation: 0, child:  Column( children: <Widget>[
                    Padding(padding: EdgeInsets.only(top: 100,)),
                    SizedBox(width: 700,),
                    Image( width: 650, image: AssetImage('assets/jon.jpg'))
                  ],),),
                  SizedBox(height: 50,),
                  Column(crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[
                    Padding(padding: EdgeInsets.only(top: 325)),
                    Center(child:  Image(width: 200,image: AssetImage('assets/dom.jpg')),)

                  ],),
                  Column(children: <Widget>[
                    Padding(padding: EdgeInsets.only(left: 720,top: 550)),
                    Builder(builder: (context)=> ButtonTheme(
                        minWidth: 200,
                        child: FlatButton(child: Text('SIGN IN'), shape: RoundedRectangleBorder(borderRadius:
                        BorderRadius.circular(20)), color:Colors.green,onPressed: () {
                          Navigator.push(context, MaterialPageRoute(builder: (context)=> SignInPage()));
                        }

                        ))
                    )],)


                ]),)
        ));
  }
}
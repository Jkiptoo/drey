import 'package:drey/MyLibrary.dart';
import 'package:flutter/material.dart';


class SignInPage extends StatefulWidget {
  @override
  _SignInPageState createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> {

  String _name;
  String _password;

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  Widget _buildName(){
    return TextFormField(
      decoration: InputDecoration(
          labelText: 'Username'
      ),
      validator: (String value) {
        if (value.isEmpty) {
          return 'Name is required';
        }
        if(value.length < 3) {
          return 'Name must be more than Three Characters';
        }
        return null;
      },
      onSaved: (String value){
        _name = value;
      },
    );

  }
  Widget _buildPassword(){
    return TextFormField(
      decoration: InputDecoration(
        labelText: 'Password',
      ),
      validator: (String value){
        if(value.isEmpty){
          return 'Password is required';
        }
        if (value.length < 8) {
          return 'Must be more than Eight charaters';
        }

        return null;
      },
      onSaved: (String value){
        _password = value;
      },
      obscureText: true,
    );
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image(image: AssetImage('assets/jon.jpg')),
              Image(image: AssetImage('assets/dom.jpg')),

              Form(
                  key: _formKey,
                  child: Column(children: <Widget>[
                    Padding(padding: EdgeInsets.all(40.0),child: Column(children: <Widget>[
                      _buildName(),
                      _buildPassword(),
                    ],),),

                    SizedBox(height: 50,),
                    Row( children: <Widget>[
                      Padding(padding: EdgeInsets.only(left: 250.0)),
                      FlatButton(onPressed: (){}, child: Text('Forgot Password')),
                    ],),

                    ButtonTheme(
                      minWidth: 150,
                      child:  FlatButton( color: Colors.teal, shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)), onPressed: (){
                        if (!_formKey.currentState.validate()) {
                          return;
                        }

                        _formKey.currentState.save();

                        print(_name);
                        print(_password);
                        Navigator.push(context, MaterialPageRoute(builder: (context)=> MyLibrary()));
                      }, child: Text('SIGN IN')),
                    )

                  ],))
            ],
          ),
        ),
      ),
    );
  }
}



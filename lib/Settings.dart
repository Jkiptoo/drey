import 'package:flutter/material.dart';


class Settings extends StatelessWidget {


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Book Details'),
          backgroundColor: Colors.teal[900],
        ),
        body: Column(children: <Widget>[

          Container(
              height: 300,

              child: Card(
                elevation: 50,
                child: Row(
                  children: <Widget>[
                    Container(child: Column(children: <Widget>[
                      Padding(padding: EdgeInsets.only(top: 10)),
                      Image(height: 200, image: AssetImage('assets/alone.jpg'))
                    ],),),
                    SizedBox(width: 30,),
                    Column(crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text('You Are Not Alone', style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.w900),),
                        Text('Greer Hendricks'),
                        SizedBox(height: 20,),
                        Text('The electrifying number one New York ',
                          style: TextStyle(fontSize: 10),),
                        Text('Times best-selling authors of An ',
                            style: TextStyle(fontSize: 10)),
                        Text('Anonymous Girl and The Wife Between ',
                            style: TextStyle(fontSize: 10)),
                        Text('Us return with a brand new novel of ',
                            style: TextStyle(fontSize: 10)),
                        Text('psychological suspense.Shay Miller',
                            style: TextStyle(fontSize: 10)),
                        Text('wants to find love, but it eludes her.',
                            style: TextStyle(fontSize: 10)),
                        Text('She wants to be fulfilled, but her job',
                            style: TextStyle(fontSize: 10)),
                        Text('is a dead end. She wants to belong, but',
                            style: TextStyle(fontSize: 10)),
                        Text('her life is increasingly lonely.',
                            style: TextStyle(fontSize: 10)),
                        SizedBox(height: 20,),
                        Text('Category: Drama'),

                        Column(crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            SizedBox(width: 200,),
                            ButtonTheme(
                              minWidth: 100,
                              child: FlatButton(color: Colors.amber,
                                  onPressed: () {},
                                  child: Text('Mark as lost')),
                            )

                          ],)
                      ],)
                  ],
                ),
              )
          ),
          Column(
              children: <Widget>[
                Image(image: AssetImage('assets/vee.png')),
                SizedBox(height: 10,),
                FlatButton(color: Colors.teal[800],
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15)),
                    onPressed: () {},
                    child: Text('Generate Qr Sticker')),
                SizedBox(height: 40,),
                BottomNavigationBar(items: [
                  BottomNavigationBarItem(title: Text(''), icon: Material(
                    child: InkWell(onTap: () {},
                      child: Image(
                          height: 40, image: AssetImage('assets/home.png')),),
                  )),
                  BottomNavigationBarItem(title: Text(''), icon: Material(
                    child: InkWell(onTap: () {},
                      child: Image(height: 40,
                          image: AssetImage('assets/library.png')),),
                  )),
                  BottomNavigationBarItem(title: Text(''), icon: Material(
                    child: InkWell(onTap: () {},
                      child: Image(
                          height: 40, image: AssetImage('assets/add.png')),),
                  ))
                ])
              ])
        ],
        )

    );
  }
}

import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';


class SettingsPage extends StatefulWidget {
  SettingsPage({this.app});
  final FirebaseApp app;
  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  final databaseReference = FirebaseDatabase.instance;
  @override
  Widget build(BuildContext context) {
    final ref = databaseReference.reference();
    final bookCategory = 'Category';
    final categoryController = TextEditingController();

    return Scaffold(
      appBar: AppBar(
        title: Text('SETTINGS'),
        backgroundColor: Colors.teal[800],
      ),
      body:
      Column(children: <Widget>[
        Container(child: Row(children: <Widget>[
          SizedBox(height: 40,),
          SizedBox(width: 10,),
          Text('Add Book Categories',style: TextStyle(
              decoration: TextDecoration.underline,fontSize: 20,fontWeight: FontWeight.bold
          ),)
        ],),),
        Container(child: Row(children: <Widget>[
          SizedBox(height: 40,),
          SizedBox(width: 20,),
          Text('Relationships'),
          SizedBox(width: 100,),
          Column(children: <Widget>[

          ],),
          Center(
            child:  RaisedButton(
              child: Icon(Icons.edit),
              onPressed: () {
                showModalBottomSheet<void>(context: context, builder: (BuildContext context) {
                  return Column(children: <Widget>[
                    Container(
                      child: Padding(
                          padding:EdgeInsets.all(32.0),
                          child: Flexible(
                            child: TextField(
                              decoration: InputDecoration(
                                  labelText: ''
                              ),
                            ),
                          )
                      ),
                    ),

                    Row(children: <Widget>[
                      SizedBox(width: 30,),
                      FlatButton(color: Colors.lightGreen, shape:RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30)
                      ), onPressed: (){
                      }, child: Text('Yes')),
                      SizedBox(width: 50,),
                      FlatButton(color: Colors.lightGreen, shape:RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20)
                      ), onPressed: (){
                        Navigator.pop(context);
                      }, child: Text('No'))
                    ],)
                  ],);

                });
              },
            ),)



        ],),),
        Container(child: Row(children: <Widget>[
          SizedBox(height: 40,),
          SizedBox(width: 20,),
          Text('Business Books',style: TextStyle(fontWeight: FontWeight.bold),),
          SizedBox(width: 85,),
          IconButton(icon: Icon(Icons.delete,color: Colors.black,), onPressed: (){})

        ],)),
        Container(child: Row(children: <Widget>[
          SizedBox(height: 40,),
          SizedBox(width: 20,),
          Text('Science Fiction',style: TextStyle(fontWeight: FontWeight.bold),),
          SizedBox(width: 91,),
          IconButton(icon: Icon(Icons.delete, color: Colors.black,), onPressed: (){})

        ],),),
        Container(child: Row(children: <Widget>[
          SizedBox(height: 40,),
          SizedBox(width: 20,),
          Text('Christian Literature',style: TextStyle(fontWeight: FontWeight.bold),),
          SizedBox(width: 65,),
          IconButton(icon: Icon(Icons.delete, color: Colors.black,), onPressed: (){})
        ],)),
        SizedBox(height: 50,),
        Container(child: Row(children: <Widget>[
          SizedBox(width: 10,),
          Text('Book ID Prefix',style: TextStyle(decoration: TextDecoration.underline, fontWeight: FontWeight.bold,fontSize: 20),)
        ],),),
        Container(child: Row(children: <Widget>[
          SizedBox(height: 40,),
          SizedBox(width: 20,),
          Text('KJM'),
          SizedBox(width:160,),
          Column(children: <Widget>[
            Center(
              child:  RaisedButton(
                child: Icon(Icons.edit),
                onPressed: () {
                  showModalBottomSheet<void>(

                      context: context,
                      builder: (BuildContext context) {
                        return Column(children: <Widget>[
                          Container(
                            child: Padding(
                                padding:EdgeInsets.all(22.0),
                                child: Flexible(
                                  child: TextField(
                                        controller: categoryController,
                                        textAlign: TextAlign.center,
                                  ),
                                )
                            ),
                          ),

                          Row(children: <Widget>[
                            SizedBox(width: 30,),
                            FlatButton(color: Colors.lightGreen, shape:RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(30)
                            ), onPressed: (){
                              ref.child('Category')
                                  .push()
                                  .set(categoryController.text)
                                  .asStream();
                              categoryController.clear();
                            }, child: Text('Yes')),
                            SizedBox(width: 50,),
                            FlatButton(color: Colors.lightGreen, shape:RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20)
                            ), onPressed: (){
                              Navigator.pop(context);
                            }, child: Text('No'))
                          ],)
                        ],);

                      });
                },
              ),)

          ],)
        ],),),
      ],),
    );
  }
}



import 'dart:io';
import 'package:firebase_database/firebase_database.dart';
import 'package:select_form_field/select_form_field.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:path/path.dart';




class AddBookLib extends StatelessWidget{

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: AddBook(),
    ) ;
  }
}


class AddBook extends StatefulWidget {
  AddBook ({this.app});
  final FirebaseApp app;
  @override
  _AddBookState createState() => _AddBookState();
}

class _AddBookState extends State<AddBook> {
  final databaseReference = FirebaseDatabase.instance;

  File _imageFile;

  final picker = ImagePicker();

  Future pickImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);

    setState(() {
      _imageFile = File(pickedFile.path);
    });
  }

  Future uploadImageToFirebase(BuildContext context) async {
    String fileName = basename(_imageFile.path);
    Reference firebaseStorageRef =
    FirebaseStorage.instance.ref().child('uploads/$fileName');
    UploadTask uploadTask = firebaseStorageRef.putFile(_imageFile);
    TaskSnapshot taskSnapshot = await uploadTask;
    taskSnapshot.ref.getDownloadURL().then(
          (value) => print("Done: $value"),
    );
  }
  final Future<FirebaseApp> initialization = Firebase.initializeApp();
  final List<Map<String, dynamic>>  _items = [
    {
      'value': 'Relationships',
      'label': 'Relationships'
    },
    {
      'value': 'Business Books',
      'label': 'Business Books'
    },
    {
      'value': 'Science Fiction',
      'label': 'Science Fiction'
    },
    {
      'value': 'Christian Literature',
      'label': 'Christian Literature'
    }
  ];
  @override
  Widget build(BuildContext context) {
    final ref = databaseReference.reference();
    final bookName = 'Book Title';
    final bookController = TextEditingController();


    return Scaffold(
      appBar: AppBar(
        title: Text('ADD BOOK TO LIBRARY'),
        backgroundColor: Colors.teal[800],
      ),
      body: SingleChildScrollView(child:
      Column(children: <Widget>[
        Row(children: <Widget>[
          Padding(padding: EdgeInsets.all(10)),
          Text(
            bookName,
            textAlign: TextAlign.center,
            style: TextStyle( fontWeight: FontWeight.bold),
          ),
          Flexible(child: Padding(padding: EdgeInsets.only(left:20.0, right: 20.0),
            child: TextFormField(
              decoration: InputDecoration(
                  labelText: ''
              ),
            ),)),


        ],),
        Row(children: <Widget>[
          Padding(padding: EdgeInsets.all(10)),
          Text('Author'),
          Flexible(child: Padding(padding: EdgeInsets.only(left:35.0, right: 20.0),
            child: TextFormField(
              decoration: InputDecoration(
                  labelText: ''
              ),
            ),))
        ],),
        SizedBox(height: 12,),
        Row(children: <Widget>[

          Padding(padding: EdgeInsets.all(10),),
          Text('Category'),
          SizedBox(width: 10,),
          Flexible(child: Padding(padding: EdgeInsets.only(left:10.0, right: 20.0),
            child: SelectFormField(

              items: _items,
              onChanged: (value)=> print(value),
              onSaved: (value)=> print(value),
            ),)),

        ],),

        SizedBox(height: 15,),
        Column(children: <Widget>[
          Row(children: <Widget>[
            SizedBox(width: 20,),
            Flexible(child: Text('Description', ))],),
          Container(height: 100, width: 350, child:

          Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
            Padding(padding: EdgeInsets.only()),
            Flexible( child: TextFormField(
              keyboardType: TextInputType.multiline,
              maxLines: null,
              decoration: InputDecoration(
                  labelText: '',
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(1)
                  )
              ),

            ),),
          ],)
          )
        ],),
        SizedBox(height: 20,),
        Row(children: <Widget>[
          SizedBox(width: 20,),
          Text('Cover Image'),

        ],),
        SizedBox(height: 10,),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Align(
              alignment: Alignment.center,
              child:  Container(
                height: 200,
                margin: const EdgeInsets.only(
                    left: 30.0, right: 30.0, top: 10.0),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(30.0),
                  child: _imageFile != null
                      ? Image.file(_imageFile)
                      : FlatButton(
                    child: Icon(
                      Icons.add_a_photo,
                      size: 50,
                    ),
                    onPressed: pickImage,
                  ),
                ),
              ),
            ),
          ],
        ),
        SizedBox(
          height: 20.0,
        ),
        SizedBox(height: 15,),
        Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
          FlatButton(color: Colors.teal[900], shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
              onPressed: ()=>  {
            ref.child('Books')
           .push()
           .set(bookController.text)
           .asStream(),
            bookController.clear()
              }, child: Text('Submit Book Details'))
        ],)
      ])
      )
    );
  }
}



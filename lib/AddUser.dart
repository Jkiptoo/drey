import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';


class CustomData extends StatefulWidget {
  CustomData({this.app});
  final FirebaseApp app;
  @override
  _CustomDataState createState() => _CustomDataState();
}

class _CustomDataState extends State<CustomData> {
  final databaseReference = FirebaseDatabase.instance;
  @override
  Widget build(BuildContext context) {
    final ref = databaseReference.reference();

    final movieName = 'MovieTitle';
    final movieController = TextEditingController();

    return Scaffold(
        appBar: AppBar(
          title: Text('Movies'),
        ),
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Center(
                child: Container(
                    color: Colors.green,
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height,
                    child: Column(
                      children: <Widget>[
                        Text(
                          movieName,
                          textAlign: TextAlign.center,
                          style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                        ),
                        TextField(
                          controller: movieController,
                          textAlign: TextAlign.center,
                        ),
                        FlatButton(color: Colors.grey, onPressed: (){
                          ref.child('Movies')
                              .push()
                              .set(movieController.text)
                              .asStream();
                          movieController.clear();
                        }, child: Text('Save Movie'),textColor: Colors.white,)
                      ],
                    )

                ),
              )
            ],
          ),
        )
    );
  }
}

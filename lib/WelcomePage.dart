import 'package:drey/SignIn.dart';
import 'package:flutter/material.dart';

class WelcomePage extends StatefulWidget {
  @override
  _WelcomePageState createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(

        child: Column(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
          SizedBox(width: 500,),
          SizedBox(height: 200,),

          Image( image: AssetImage('assets/jon.jpg')),
          Material(child:
          InkWell(
            onTap: (){
              Navigator.push(context, MaterialPageRoute(builder: (context)=> SignIn()));
            },
            child: Image(image: AssetImage('assets/dom.jpg')),
          ),),


          Column(mainAxisAlignment: MainAxisAlignment.end, children: <Widget>[
            SizedBox(height: 203,),
            Image(image: AssetImage('assets/dashboard.png')),
          ],)
        ],),
      ),
    );
  }
}


import 'package:flutter/material.dart';
import 'package:select_form_field/select_form_field.dart';

class BookLibrary extends StatelessWidget {
  final String title;

  BookLibrary({Key key, this.title}) : super(key: key);
  final List<Map<String, dynamic>> _items = [
    {
      'value': 'Relationships',
      'label': 'Relationships'
    },
    {
      'value': 'Business Books',
      'label': 'Business Books'
    },
    {
      'value': 'Science Fiction',
      'label': 'Science Fiction'
    },
    {
      'value': 'Christian Literature',
      'label': 'Christian Literature'
    }
  ];


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:  Text('Book LIbrary'),
        backgroundColor: Colors.teal[900],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SizedBox(height: 30,),
            Container(
              height: 50,
              width: 400,
              child: Card(
                elevation: 10,
              child: Padding(padding: EdgeInsets.all(0.0),
              child: Row(mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  SizedBox(width: 10,),
                Flexible(child: TextFormField(
                  decoration: InputDecoration.collapsed(
                    hintText: 'Search here'
                  ),
                )),
                   SizedBox(width: 20,),
                  IconButton(onPressed: (){}, icon: Icon(Icons.search),),


              ],),),
            ),),
              Row(children: <Widget>[
                Padding(padding: EdgeInsets.all(10)),
                Text('Filter by category',style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),),
                SizedBox(width: 20,),
                Flexible(child: SelectFormField(
                  items: _items,
                  onChanged: (value)=> print(value),
                  onSaved: (value)=> print(value),
                ),),
                SizedBox(width: 20,)
              ],),
            SizedBox(height: 20,),
            Row(
              children: <Widget>[
                Row(children: <Widget>[

                ],),
                SizedBox(width: 10,),
                Column(children: <Widget>[
                  Image(height: 180,  image: AssetImage('assets/alone.jpg')),
                  Text('You are not Alone',style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold),),
                  Text('Greer Hendricks')
                ],),
                SizedBox(width: 20,),
                Column(children: <Widget>[
                  Image(height: 180, image: AssetImage('assets/mee.jpg')),
                  Text('After',style: TextStyle(fontWeight: FontWeight.bold,fontSize: 15),),
                  Text('Anna Todo')
                ],),
                SizedBox(width: 20,),
                Column(children: <Widget>[
                  Image(height: 180, image: AssetImage('assets/extra.jpg')),
                  Text('Flood',style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold),),
                  Text('Erik Drocker')
                ],)
              ],
            )
          ],
        ),
      ),
    );
  }
}
